module Statsy
  class Api < Padrino::Application

    register Padrino::Warden

    disable :sessions

    set :warden_failure_app, ApiFailureApp.new

    Rabl.register!

    Rabl.configure do |config|
      config.include_json_root = false
      config.include_child_root = false
    end
  end
end

require 'active_support/core_ext/numeric/time'

Statsy::Api.controllers :matches, provides: :json do
  get :index do
    results = Store.db.table(:matches).filter do |m|
      m[:team_blue].eq(nil).not.or(m[:team_red].eq(nil).not)
    end.order_by('match_creation').map do |match|
      {
        match_creation: match[:match_creation],
        match_id: match[:match_id],
        match_duration: match[:match_duration],
        team_blue: Store.db.table(:teams).get(match[:team_blue]),
        team_purple: Store.db.table(:teams).get(match[:team_purple])
      }
    end.run.to_a

    @teams = Store.db.table(:teams).order_by(:name).run.to_a.map do |team|
      Team.new(team)
    end

    @matches = results.map { |result| MatchDetail.new(result) }

    render :rabl, :'matches/index.json'
  end

  delete :index, :with => :id do
    id = params[:id].to_i

    @match = Store.db.table(:matches).get_all(id, { index: :match_id }).delete.run

    if @match.fetch('deleted') == 0
      halt(404, MultiJson.dump({ status: 404, message: 'resource not found' }))
    else
      MultiJson.dump({ status: 200 })
    end
  end

  post :new do
    store = Store.db

    match_id = params[:match].to_i
    team_blue = params[:teamBlue]
    team_purple = params[:teamPurple]

    query = store.table(:matches).get_all(match_id, { index: :match_id })
    exists = query[:match_id].contains(match_id).run

    if exists
      @match = []
    elsif match_id > 0
      api = Riotic::API.new(
        ENV['RIOT_API_KEY'],
        ENV['RIOT_REGION'],
        Riotic::RedisStore.new
      )

      result = api.match.match(match_id)

      unless result.match_id.nil?
        dump = MultiJson.dump(result)
        Store.db.table(:matches).insert(MultiJson.load(dump).merge({
          team_blue: team_blue,
          team_purple: team_purple,
        })).run

        query = query.map do |match|
          {
            match_creation: match[:match_creation],
            match_id: match[:match_id],
            match_duration: match[:match_duration],
            team_blue: Store.db.table(:teams).get(match[:team_blue]),
            team_purple: Store.db.table(:teams).get(match[:team_purple])
          }
        end

        @match = MatchDetail.new(query.run.to_a.first)
      end
    end

    if @match
      render :rabl, :'matches/new.json'
    else
      halt(404, MultiJson.dump({ status: 404, message: 'resource not found' }))
    end
  end

  before do
    login
  end
end

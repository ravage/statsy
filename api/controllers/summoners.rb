Statsy::Api.controllers :summoners, provides: :json do
  # Returns a list of verified summoners
  get :verified do
    @summoners = Summoner.verified

    MultiJson.dump(@summoners)
  end

  # Returns a list of summoners that didn't pass validation
  # or weren't validated yet
  get :invalid do
    @summoners = Summoner.invalid

    MultiJson.dump(@summoners)
  end

  # Imports teams and summoners from our internal API
  post :import do
    InternTeamWorker.perform_async

    return MultiJson.dump({ status: :ok })
  end

  # Validates imported summoners with the Riot API
  post :validate do
    delay = 10
    iteration = 0

    summoners = Store.db.table(:summoners)
      .filter({ status: 0 }).pluck(:id, :nick).run.to_a

    total = (summoners.length / 40).ceil

    summoners.each_slice(40) do |chunk|
      iteration += 1
      wait = iteration * delay

      data = {
        batch: iteration,
        max: total.to_i,
        summoners: chunk
      }

      NameCheckWorker.perform_in(wait.seconds, data)
    end

    MultiJson.dump({ status: :ok })
  end

  # Revalidates summoners with a riot id for nick changes
  post :refresh do
    delay = 5
    iteration = 0

    summoners = Store.db.table(:summoners)
      .filter({ status: 1 }).pluck(:id, :riot_id, :revision_date).run.to_a

    total = (summoners.length / 40).ceil

    summoners.each_slice(40) do |chunk|
      iteration += 1
      wait = iteration * delay

      data = {
        batch: iteration,
        max: total.to_i,
        summoners: chunk
      }

      IdCheckWorker.perform_in(wait.seconds, data)
    end

    MultiJson.dump({ status: :ok })
  end

  # Deletes all data from tables summoners and teams
  post :reset do
    Store.db.table(:summoners).delete().run;
    Store.db.table(:teams).delete().run;

    MultiJson.dump({ status: :ok })
  end


  before do
    login
  end

end

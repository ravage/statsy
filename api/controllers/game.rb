Statsy::Api.controllers :games, provides: :json do
  get :recent, map: '/games/recent' do
    store = Store.db

    api = Riotic::API.new(
      ENV['RIOT_API_KEY'],
      ENV['RIOT_REGION'],
      Riotic::RedisStore.new
    )

    filter = lambda { |match| match.has_fields('status').not() }
    matches = store.table(:matches).filter(filter).map do |match|
      {
        id: match[:id],
        match_id: match[:match_id],
        duration: match[:match_duration],
        team_blue: match[:team_blue],
        team_purple: match[:team_purple],
        summoners: store.branch(
          match[:team_blue].eq(nil).or(match[:team_purple].eq(nil)),
          {},
          store.table(:summoners).get_all(
            match[:team_blue], match[:team_purple], { index: :team_id }
          ).pluck(:riot_id, :team).coerce_to('ARRAY')
        )
      }
    end.run.to_a

    delay = 10
    iteration = 0

    matches.each do |match|
      match['summoners'].each do |summoner|
        iteration += 1
        wait = iteration * delay

        batch = {
          batch: iteration,
          id: match['id'],
          match_id: match['match_id'],
          duration: match['duration'],
          summoner: summoner
        }

        GameAggregatorWorker.perform_in(wait.seconds, batch)
      end

      store.table(:matches).get(match['id']).update({ status:  1 }).run
    end

    MultiJson.dump({ status: :ok })

  end
end

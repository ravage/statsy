object @match => :match

attributes :match_id, :match_duration
attributes :team_blue, :team_purple

node(:match_creation) { |o| Time.at(o.match_creation / 1000).strftime('%d/%m/%Y @ %H:%M') }
node(:match_duration) { |o| "#{o.match_duration / 60} minutes" }

child @matches => :matches do
  attribute :match_id
  attribute :team_blue
  attribute :team_purple

  node(:match_creation) { |o| Time.at(o.match_creation / 1000).strftime('%d/%m/%Y @ %H:%M') }
  node(:match_duration) { |o| "#{o.match_duration / 60} minutes" }
end

child @teams => :teams do
  attributes :id, :captain, :name
end

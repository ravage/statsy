module Store
  class << self
    include RethinkDB::Shortcuts

    def logger=(logger)
      @logger ||= logger
    end

    def logger
      @logger
    end

    def db
      begin
        @store ||= r
        @store.connect(host: ENV['RDB_HOST'], port: ENV['RDB_PORT'], db: ENV['RDB_DB']).repl
        @store
      rescue Exception => err
        logger.error("RDB Exception: #{err.message}") if @logger
      end
    end
  end
end

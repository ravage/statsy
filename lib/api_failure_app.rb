class ApiFailureApp
  def call(env)
    ['401', {'Content-Type' => 'application/json'}, []]
  end
end

class SummonerStats
  attr_reader :summoner
  attr_reader :league
  attr_reader :champion_aggregate
  attr_reader :champions
  attr_reader :total_games
  attr_reader :games
  attr_reader :found

  def initialize(api)
    @api = api
    @found = false
  end

  def get(id)
    db = Store.db

    summoner = id.to_i;

    games = db.table('games').filter do |g|
      g[:players].contains do |p|
        p[:summoner_id].eq(summoner);
      end
    end.order_by(db.desc('create_date'))

    if !games.run.empty?
      @found = true
      @games = games.map do |g|
        {
          game: g.without('players'),
          player: g[:players].filter({ summoner_id: summoner }).nth(0).merge do |p|
            {
              champion_name: db.table('champions').get(p[:champion_id])[:name]
            }
          end
        }
      end

      avg_map = {
        kills: 'champions_killed',
        deaths: 'num_deaths',
        minions: 'minions_killed',
        assists: 'assists',
        gold: 'gold_earned'
      }

      sum_map = {
        tkills: 'champions_killed',
        tdeaths: 'num_deaths',
        tminions: 'minions_killed',
        tassists: 'assists',
        tgold: 'gold_earned'
      }

      @stats = {}

      stats = games.map do |g|
        g[:players].filter({ summoner_id: summoner }).nth(0)
      end

      avg_map.each do |k, v|
        @stats[k] = stats.avg do |p|
          value = p[:stats][v.to_sym]
          db.branch(value.eq(nil), 0, value)
        end.run.round(1)
      end

      sum_map.each do |k, v|
        @stats[k] = stats.sum do |p|
          value = p[:stats][v.to_sym]
          db.branch(value.eq(nil), 0, value)
        end.run
      end

      @champions = {}
      @champion_aggregate = @games.group { |g| g[:player][:champion_id] }.count().run.to_h
      @champion_aggregate = @champion_aggregate.sort_by { |k, v| v }.reverse[0..4].reverse.to_h
      @total_games = games.count.run
      @games = @games.limit(5).run.to_a
      @summoner = Summoner.new(
        db.table('summoners').get_all(summoner, { index: :riot_id }).merge do |s|
          { school: db.table('schools').filter({ team_id: s[:team][:id] }).nth(0) }
        end.nth(0).run
      )
      champions = db.table('champions').get_all(*@champion_aggregate.keys).run.to_a;
      champions.each { |champion| @champions[champion['id']] = Champion.new(champion) }

      begin
        @league = @api.league.summoner_entry(summoner).select do
          |l| l.queue == 'RANKED_SOLO_5x5'
        end.first
      rescue Riotic::NotFound
        @league = nil
      end
    else
      @found = false
    end

    self
  end

  def found?
    @found
  end

  def [](key)
    @stats[key]
  end

end

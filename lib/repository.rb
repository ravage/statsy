class Repository
  def self.for(type)
    repositories.fetch(type.to_sym)
  end

  def self.register(type, repository)
    repositories[type.to_sym, repository]
  end

  def self.repositories
    @repositories ||= {}
  end
end

module RethinkDBRepository

  module Adapter
    include RethinkDB::Shortcuts

    def self.included(base)
      base.singleton_class.send(:attr_accessor, :_table)
      base.singleton_class.send(:attr_accessor, :_klass)

      base.extend(ClassMethods)
    end

    module ClassMethods
      def table(name)
        self._table = name
      end

      def klass(name)
        self._klass = name
      end
    end

    def initialize
      @connection ||= r.connect(
        host: ENV['RDB_HOST'],
        port: ENV['RDB_PORT'],
        db: ENV['RDB_DB']
      )
    end

    def all
      store.run(connection)
    end

    def create(model)
      store.insert(model.to_h, return_changes: true).run(connection)
    end

    def delete(model)
      store.delete({ id: model.id }).run(connection)
    end

    def find(id)
      store.get(id).run(connection)
    end

    def update(model)
      store.filter({ id: model.id }).update(model.to_h, return_changes: true).run(connection)
    end

    def query(criteria)
      store.filter(criteria).run(connection)
    end

    def store
      r.table(table)
    end

    def table
      self.class._table
    end

    def klass
      self.class._klass
    end

    def connection
      @connection
    end

  end

  class UserRepository
    include RethinkDBRepository::Adapter

    klass User
    table :users

    def initialize
      super
      binding.pry
    end
  end

  class TeamRepository
    include RethinkDBRepository::Adapter

    klass Team
    table :teams

    def initialize
      super
    end
  end

end

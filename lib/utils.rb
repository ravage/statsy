module Utils
  class << self
    def normalize(text)
      text.downcase.gsub(/\s/, '')
    end

    def to_minutes(seconds)
      seconds / 60
    end

    def items_assets(stats)
      0.upto(6) do |index|
        item = "item#{index}"
        yield "#{stats[item]}" if stats[item]
      end
    end

    def get_match_outcome(result)
      result ? 'win' : 'lose'
    end

    def champion_percent(count, max)
      (count.to_f / max * 100).round
    end

    def summoner_photo(summoner)
      if File.exists?(File.join(Padrino.root, 'public', 'assets', 'frontend', "#{summoner.riot_id}.jpg"))
        "/assets/frontend/#{summoner.riot_id}.jpg"
      else
        '/assets/frontend/default.jpg'
      end
    end

    def sanitize(text)
      text.delete(' ').delete('\'').delete('.')
    end

    def cdn(path)
      if Padrino.env == :production
        "//d2sbd32x62v2qq.cloudfront.net#{path}"
      else
        path
      end
    end

    def get_asset(id, type)
      contents = File.read(File.join(Padrino.root, 'public', 'frontend-manifest.json'))
      manifest = JSON.parse(contents)
      manifest[id][type]
    end

    def number_to_human(number)
      size = {
        4 => 1000,
        5 => 10000,
        6 => 100000,
        7 => 1000000
      }

      length = number.to_i.to_s.length
      size.key?(length) ? "#{(number.to_f / size[length]).round(1)}K" : number
    end
  end
end

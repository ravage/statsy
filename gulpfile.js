/* jshint strict: false */

var gulp        = require('gulp');
var util        = require('gulp-util');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var browserify  = require('browserify');
var source      = require('vinyl-source-stream');
var uglify      = require('gulp-uglify');
var buffer      = require('vinyl-buffer');
var minifyHtml  = require('gulp-minify-html');
var handlebars  = require('gulp-handlebars');
var wrap        = require('gulp-wrap');
var declare     = require('gulp-declare');
var reactify    = require('reactify');

// var pkg         = require('./package.json');
// var deps        = Object.keys(pkg.dependencies);
var deps        = ['jquery', 'socket.io', 'react', 'ampersand-router', 'lodash', 'eventemitter3'];

var paths = {
  css: {
    input:  [
      './bower_components/skeleton/css/normalize.css',
      './bower_components/skeleton/css/skeleton.css',
      './app/assets/scss/**/*.scss'
    ],
    output: './public/css'
  },

  js: {
    input:  './app/assets/js/app.js',
    output: './public/js',
    all:    ['./app/assets/js/**/*.js', './app/assets/js/**/*.jsx']
  },

  templates: {
    input:  './app/assets/js/components/**/*.hbs',
    output: './public/js'
  },

};

var external = {
  jquery: {
    path: './bower_components/jquery/dist/jquery.js',
    expose: 'jquery'
  },

  'socket.io': {
    path: './bower_components/socket.io-client/socket.io.js',
    expose: 'socket.io'
  }
};

function errorHandler(error) {
  util.log(util.colors.red(error.message));
  this.end();
}

gulp.task('css', function() {
  return gulp.src(paths.css.input)
    .pipe(sass({ outputStyle: 'compressed', includePaths: ['bower_components'] }))
    .pipe(concat('bundle.css'))
    .pipe(gulp.dest(paths.css.output));
});

gulp.task('js', function() {
  return browserify({
      debug: true,
      entries: paths.js.input,
      transform: [reactify],
      extensions: ['.js', '.jsx'],
      paths: ['./app/assets/js/components']
    })
    .external(deps)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(paths.js.output));
});

gulp.task('vendor:js', function() {
  return browserify()
    .require(require.resolve(external.jquery.path), { expose: external.jquery.expose })
    .require(require.resolve(external['socket.io'].path), { expose: external['socket.io'].expose })
    .require('react')
    .require('ampersand-router')
    .require('lodash')
    .require('eventemitter3')
    .bundle()
    .pipe(source('vendor.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(paths.js.output));
});

gulp.task('templates', function(){
  return gulp.src(paths.templates.input)
    .pipe(minifyHtml())
    .pipe(handlebars())
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'App.templates',
      noRedeclare: true,
      processName: function(filePath) {
        return declare.processNameByPath(filePath.replace('assets/js/components', ''));
      }
    }))
    .pipe(concat('templates.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.templates.output));
});

gulp.task('default', ['css', 'vendor:js', 'js', 'templates'], function() {
  gulp.watch(paths.css.input, ['css']);
  gulp.watch(paths.js.all, ['js']);
  gulp.watch(paths.templates.input, ['templates']);
});


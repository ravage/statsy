var compileSass = require('broccoli-sass');
var concat = require('broccoli-concat');
var uglify = require('broccoli-uglify-js');
var mergeTrees = require('broccoli-merge-trees');
var pickFiles = require('broccoli-static-compiler');
var app = 'app';

var fonts = pickFiles(app, {
  srcDir: '/assets/vendor/font-awesome/fonts',
  files: ['*'],
  destDir: '/fonts'
});

var images = pickFiles(app, {
  srcDir: '/assets/images',
  files: ['**/*.png', '**/*.jpg'],
  destDir: '/images'
});

var vendor = concat(app, {
  inputFiles: ['assets/vendor/**/*.js'],
  outputFile: '/js/vendor.js',
  header:     '/* vendor */'
});

vendor = uglify(vendor, { compress: true });

var sass = compileSass(
  [app, 'app/assets'],
  'assets/scss/app.scss',
  '/css/bundle.css',
  { outputStyle: 'compressed' }
);

module.exports =  mergeTrees([vendor, sass, fonts, images]);

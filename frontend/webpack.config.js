var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var AssetsPlugin = require('assets-webpack-plugin');
var Clean = require('clean-webpack-plugin');

module.exports = {
  entry: {
    frontend: './assets/js/app'
    // vendor: ['ampersand-router', 'lodash', 'q', 'reflux', 'riot', 'superagent', 'revalidator', 'numeral']
  },
  output: {
    filename: '[name]-bundle-[hash].js',
    path: '../public/assets'
  },
  module: {
    loaders: [
      {
        test: /\.tag$/,
        loader: 'tag',
        query: { type: 'es6' },
        include: [path.join(__dirname, 'assets/js')]
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: [path.join(__dirname, 'assets/js')]
      },
      {
        test: /\.scss|\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader' +
                                          '?sourceMap&includePaths[]=' + (path.resolve(__dirname, 'bower_components')))
      },
      { test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$/, loader: 'file' },
    ]
  },
  plugins: [
    new Clean(['../public/assets/frontend-bundle-*']),
    new ExtractTextPlugin("[name]-bundle-[hash].css"),
    new AssetsPlugin({
      path: path.join(__dirname, '../public'),
      filename: 'frontend-manifest.json'
    })
    // new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js')
  ]
};

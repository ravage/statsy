Statsy::Frontend.controllers do
  get :index do
    summoner = Store.db.table('games')[:players].sample(1).nth(0).nth(0)[:summoner_id].run
    @stats = SummonerStats.new(@api).get(summoner)

    render 'frontend/index'
  end

  post :search do
    summoner_name = params.fetch('summoner', nil)

    summoner = Store.db.table('summoners').filter do |s|
      s[:nick].match("(?i)#{summoner_name}$")
    end.run.to_a

    if summoner.empty?
      @stats = SummonerStats.new(@api)
      render 'frontend/index'
    else
      p summoner.first['riot_id']
      redirect summoner.first['riot_id']
    end
  end

  get :show, map: '',  with: :id do
    @stats = SummonerStats.new(@api).get(params[:id])

    render 'frontend/index'
  end


  before do
    @api = Riotic::API.new(
      ENV['RIOT_API_KEY'],
      ENV['RIOT_REGION'],
      Riotic::RedisStore.new
    )
  end

end

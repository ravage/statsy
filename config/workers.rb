path = File.expand_path(File.join(File.dirname(__FILE__), '..'))

$LOAD_PATH.unshift(path) unless $LOAD_PATH.include?(path)

require 'dotenv'

Dotenv.load(".env.#{Sidekiq.options[:environment]}", '.env.api')

require 'intern'
require 'riotic'
require 'rethinkdb'
require 'pry'
require 'warden'
require 'bcrypt'

require 'lib/utils'
require 'lib/store'
require 'lib/notifier'
require 'app/models/team'
require 'app/models/user'
require 'app/workers/intern_team_worker'
require 'app/workers/name_check_worker'
require 'app/workers/id_check_worker'
require 'app/workers/game_aggregator_worker'

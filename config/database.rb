RDB = RethinkDB::RQL.new

connection = RDB.connect(host: ENV['RDB_HOST'], port: ENV['RDB_PORT'])

tables = {
  summoners: [:riot_id],
  users: [:email],
  matches: [:match_id],
  games: [],
  schools: [:team_id],
  champions: [],
  exceptions: []
}

begin
  RDB.db_create(ENV['RDB_DB']).run(connection)
rescue RethinkDB::RqlRuntimeError
  logger.info("Database #{ENV['RDB_DB']} already exists.")
end

tables.keys.each do |table|
  begin
    RDB.db(ENV['RDB_DB']).table_create(table).run(connection)
  rescue RethinkDB::RqlRuntimeError
    logger.info("Table #{table} already created!")
  end
end

tables.each do |table, indexes|
  indexes.each do |index|
    begin
      RDB.db(ENV['RDB_DB']).table(table).index_create(index).run(connection)
    rescue RethinkDB::RqlRuntimeError
      logger.info("Index #{ENV['RDB_DB']} - #{table} - #{index} already created!")
    end
  end
end

begin
  RDB.db(ENV['RDB_DB']).table(:summoners).index_create(:team_id) do |s|
    s[:team][:id]
  end.run(connection)
rescue RethinkDB::RqlRuntimeError
  logger.info("Index #{ENV['RDB_DB']} - summoners - [team][id] already created!")
end

connection.close

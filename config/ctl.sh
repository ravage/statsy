#!/bin/sh

app_path=/srv/sites/hub.legends.ismai.pt
user=deployer

case "$1" in
        'start')
                echo "Starting Statsy"
                su - -c "cd $app_path; bundle exec pumactl -F config/puma.rb start" $user
                su - -c "cd $app_path; bundle exec sidekiq -d -e production -P ./tmp/pids/sidekiq.pid -r ./config/workers.rb -L ./log/sidekiq.log -c 10" $user
        ;;
        'stop')
                echo "Stopping Statsy"
                su - -c "cd $app_path; bundle exec pumactl -F config/puma.rb stop" $user
                su - -c "cd $app_path; bundle exec sidekiqctl stop ./tmp/pids/sidekiq.pid" $user
        ;;
        'restart')
                echo "Restarting Statsy"
                su - -c "cd $app_path; bundle exec pumactl -F config/puma.rb restart" $user
                su - -c "cd $app_path; bundle exec sidekiqctl stop ./tmp/pids/sidekiq.pid" $user
                su - -c "cd $app_path; bundle exec sidekiq -d -e production -P ./tmp/pids/sidekiq.pid -r ./config/workers.rb -L ./log/sidekiq.log -c 10" $user
        ;;
esac

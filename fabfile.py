from fabric.api import run, task, local, cd, env
from fabric.contrib import files
from fabric.operations import put
from fabric.context_managers import shell_env

env.hosts = ['hub.legends.ismai.pt']
env.user = 'deployer'
env.path = '/home/deployer/.gem/ruby/2.2.0/bin'

deployer = 'deployer'

repo_path = '/home/deployer/repos/hub.legends.ismai.pt.git'
repo = 'deploy'
deploy_path = '/srv/sites/hub.legends.ismai.pt'
production_files = {
    '.env.production': '.env.production',
    '.env.api': '.env.api'
}


@task
def setup():
    mkdir(repo_path)
    mkdir(deploy_path)
    git_init()
    push()
    clone()

    with cd(deploy_path):
        run('gem install bundler')
        run('mkdir -p tmp/{sockets,pids}')
        run('mkdir -p log')


@task
def push():
    local('git push {0} master'.format(repo))


@task
def clone():
    if not files.exists('{0}/.git'.format(deploy_path)):
        run('git clone {repo} {deploy}'.format(
            repo=repo_path, deploy=deploy_path))


@task
def pull():
    with cd(deploy_path):
        run('git pull')


@task
def copy_production_files():
    with cd(deploy_path):
        for key, value in production_files.iteritems():
            put(key, value, mode=0600)


@task
def bundle():
    with cd(deploy_path):
        run('bundle install --path vendor/bundle')

@task
def reload_server():
    with cd(deploy_path):
        run('bundle exec pumactl -F config/puma.rb restart')


@task
def start_sidekiq():
    with cd(deploy_path):
        run('bundle exec sidekiq -d -e production -P ./tmp/pids/sidekiq.pid -r ./config/workers.rb -L ./log/sidekiq.log -c 10', pty=False, shell=True)


@task
def stop_sidekiq():
    with cd(deploy_path):
        run('bundle exec sidekiqctl stop ./tmp/pids/sidekiq.pid')


def mkdir(path):
    if not files.exists(path):
        run('mkdir -p {0}'.format(path))

def git_init():
    if not files.exists('{0}/config'.format(repo_path)):
        with cd(repo_path):
            run('git init --bare')

@task
def deploy():
    push()
    pull()
    copy_production_files()
    bundle()
    reload_server()
    stop_sidekiq()
    start_sidekiq()

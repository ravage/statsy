Statsy::App.helpers do
  def active(pattern)
    'active' if Regexp.new(pattern).match(request.path)
  end
end

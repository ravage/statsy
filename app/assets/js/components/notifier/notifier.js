(function() {
  'use strict';

  var _ = require('lodash');
  var $ = require('jquery');
  var Emitter = require('eventemitter3');

  var notifier = {
    emitter: new Emitter(),

    template: _.template('<li class="alert-box <%= category %>"><i class="alert-icon fa fa-<%= category %>"></i> <%= message %></li>'),

    container: $('.alert-box-container > ul'),

    notify: function(category, message) {
      var element = $(this.template({ category: category, message: message }));

      this.container.append(element);

      element.delay(5000).fadeOut(function() {
        this.remove();
      });
    },

    emit: function(event, message) {
      this.emitter.emit(event, message);
    },

    on: function(event, listener) {
      this.emitter.on(event, listener);
    }
  };

  module.exports = notifier;
})();

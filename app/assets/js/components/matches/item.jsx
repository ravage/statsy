(function() {
  'use strict';

  var React = require('react');

  var Component = React.createClass({
    render: function() {
      return (
        <tr>
          <td>{this.props.match.match_id}</td>
          <td>{this.props.match.match_creation}</td>
          <td>{this.props.match.match_duration}</td>
          <td>{this.props.teamBlue.name} <span className="vs">vs</span> {this.props.teamPurple.name}</td>
        </tr>
      );
    }
  });

  module.exports = Component;

})();

(function() {
  'use strict';

  var React = require('react');
  var $ = require('jquery');
  var notifier = require('notifier/notifier');
  var MatchItem = require('matches/item.jsx');

  var Component = React.createClass({
    getInitialState: function() {
      return {
        matches: [],
        teams: [],
        teamPurple: { name: '' },
        teamBlue: { name: '' }
      };
    },

    componentDidMount: function() {
      $.get(this.props.url, function(response) {
        this.setState({
          matches: response.matches,
          teams: response.teams
        });
      }.bind(this));
    },

    deleteHandler: function(match) {
      $.ajax({
        type: 'DELETE',
        url: ['/api/matches', match.match_id].join('/'),
        success: function(result) {
          var matches = this.state.matches.filter(function(m) {
            return m.match_id !== match.match_id;
          });

          this.setState({ matches: matches });
        }.bind(this),
        dataType: 'json'
      });
    },

    addHandler: function(e) {
      var match = this.refs.matchId.getDOMNode().value;

      if ((e.charCode === 13 || e.charCode === undefined) && match.trim() !== '') {

        var promise = $.post('/api/matches/new', {
          match: match,
          teamBlue: this.state.teamBlue.id,
          teamPurple: this.state.teamPurple.id
        });

        promise.fail(function() {
          notifier.notify('error', 'Match ID not found or API sever not responding!');
        });

        promise.done(function(response) {
          this.setState({ matches: this.state.matches.concat(response) });
          match = "";
        }.bind(this));
      }
    },

    onTeamRedChangeHandler: function(e) {
      this.state.teamPurple = this.state.teams[e.target.value];
    },

    onTeamBlueChangeHandler: function(e) {
      this.state.teamBlue = this.state.teams[e.target.value];
    },

    render: function() {
      var matches = function(match, teamPurple, teamBlue) {
        return <MatchItem
          key={match.match_id}
          match={match}
          teamPurple={match.team_purple}
          teamBlue={match.team_blue}
          deleteHandler={this.deleteHandler.bind(this, match)} />;
      }.bind(this);

      var teams = function(team, id) {
        return <option key={team.id} value={id}>{team.name}</option>;
      }.bind(this);

      return (
        <div className="matches">
          <h1>Matches</h1>
          <input
            name="match-id"
            type="number"
            ref="matchId"
            placeholder="match id"/>

          <select
            className="inline"
            name="team-blue"
            ref="teamBlue"
            onChange={this.onTeamBlueChangeHandler}>
            <option value="0">Team Blue</option>
            {this.state.teams.map(teams)}
          </select>

          <select
            className="inline"
            name="team-purple"
            ref="teamPurple"
            onChange={this.onTeamRedChangeHandler}>
            <option value="0">Team Purple</option>
            {this.state.teams.map(teams)}
          </select>


          <button className="button-primary inline" onClick={this.addHandler}>
            <i className="fa fa-plus"></i>
          </button>


          <table className="u-full-width">
            <thead>
              <th>Id</th>
              <th>Created</th>
              <th>Duration</th>
              <th>Challengers</th>
            </thead>
            <tbody>
              { this.state.matches.map(function(match) {
                return matches(match, this.state.teamPurple, this.state.teamBlue);
              }.bind(this))}
            </tbody>
          </table>
        </div>
      );
    }
  });

  module.exports = Component;
})();

(function() {
  var React = require('react');

  var Component = React.createClass({
    render: function() {
      return (
        <tr>
          <td>{ this.props.summoner.riot_id }</td>
          <td>{ this.props.summoner.nick }</td>
          <td>{ this.props.summoner.team.name }</td>
        </tr>
      );
    }
  });

  module.exports = Component;
})();

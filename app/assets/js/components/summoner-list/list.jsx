(function() {
  'use strict';

  var React = require('react');
  var SummonerItem = require('summoner-list/item');

  var Component = React.createClass({
    render: function() {
      var summoners = function (summoner) {
        return <SummonerItem key={summoner.id || summoner.name} summoner={summoner} />;
      };

      return (
        <table className="u-full-width">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nick</th>
                    <th>Team</th>
                </tr>
            </thead>
            <tbody>
            { this.props.summoners.map(summoners) }
            </tbody>
        </table>
      );
    }
  });

  module.exports = Component;
})();

(function() {
  'use strict';

  var React = require('react');
  var $ = require('jquery');
  var SummonerList = require('summoner-list/list');
  var notifier = require('notifier/notifier');

  var Component = React.createClass({
    getInitialState: function() {
      return {
        summoners: [],
        validating: false,
        importing: false,
        refreshing: false
      };
    },

    componentDidMount: function() {
      $.ajax({
        url: this.props.url,
        dataType: 'json',
        success: function(response) {
          this.setState({ summoners: response });
        }.bind(this)
      });

      notifier.on('jobs', this.jobHandler);
    },

    jobHandler: function(data) {
      this[[data.job, 'Handler'].join('')](data);
    },

    internHandler: function(data) {
      this.setState({
        importing: !data.finished
      });
    },

    nameCheckHandler: function(data) {
      this.setState({
        validating: !data.finished
      });
    },

    idCheckHandler: function(data) {
      this.setState({
        refreshing: !data.finished
      });
    },

    importHandler: function() {
      $.post('/api/summoners/import');

      notifier.notify('message', 'The minions are importing the data!');
      this.setState({ importing: true });
    },

    validateHandler: function() {
      $.post('/api/summoners/validate');
      notifier.notify('message', 'The minions are validating summoners!');
      this.setState({ validating: true });
    },

    refreshHandler: function() {
      $.post('/api/summoners/refresh');
      notifier.notify('message', 'The minions are refreshing summoner data!');
      this.setState({ refreshing: true });
    },

    deleteHandler: function() {
      $.post('/api/summoners/reset', function() {
        this.setState({ summoners: [] });
      }.bind(this));
    },

    render: function() {
      return (
        <div className="summoner-list-component">
          <div className='group'>
            <h1 className="u-pull-left">{this.props.title}</h1>

            <button onClick={this.deleteHandler}
              disabled={!!!this.state.summoners.length}
              className="button small error u-pull-right"
              title="Removes all entries regardng summoners and teams.">
              <i className="fa fa-times fa-fw"></i> Reset
            </button>

            <button onClick={this.refreshHandler}
              disabled={this.state.refreshing}
              className="button small safe u-pull-right"
              title="Validates all verified summoners by id and changes the name accordingly.">
              <i className="fa fa-refresh fa-fw"></i> Refresh
            </button>

            <button onClick={this.validateHandler}
              disabled={this.state.validating}
              className="button small safe u-pull-right"
              title="Validates unverified summoners by name.">
              <i className="fa fa-check fa-fw"></i> Validate
            </button>

            <button disabled={this.state.importing}
              onClick={this.importHandler}
              className="button small safe u-pull-right"
              title="Imports summoners and teams from the internal API.">
              <i className="fa fa-cloud-download fa-fw"></i> Import
            </button>

          </div>
          <SummonerList summoners={this.state.summoners}/>
        </div>
      );
    }
  });

  module.exports = Component;
})();

(function() {
  'use strict';

  var $ = require('jquery');
  var io = require('socket.io')();
  var AmpersandRouter = require('ampersand-router');
  var React = require('react');

  var spinner = $('.spinner');
  var notifier = require('notifier/notifier');
  var SummonerListComponent = require('summoner-list/component');
  var MatchesComponent = require('matches/component');

  $(document).ajaxStart(function() {
    spinner.show();
  });

  $(document).ajaxComplete(function() {
    spinner.hide();
  });

  $(document).ajaxSend(function(e, xhr, options) {
    var token = $('meta[name=csrf-token]').attr('content');

    if (options.type === 'POST' || options.type === 'PUT' || options.type === 'DELETE') {
      xhr.setRequestHeader('X-CSRF-Token', token);
    }
  });

  var Router = AmpersandRouter.extend({
    routes: {
      'manage/summoners/verified':   'verified',
      'manage/summoners/invalid':    'invalid',
      'manage/matches':              'matches'
    },

    verified: function() {
      var selector = '[data-component=summoner-list-component]';
      var element = document.querySelector(selector);
      var component = <SummonerListComponent
        router={this}
        url='/api/summoners/verified'
        title='Verified Summoners'/>;

      React.render(component, element);
    },

    invalid: function() {
      var selector = '[data-component=summoner-list-component]';
      var element = document.querySelector(selector);
      var component = <SummonerListComponent
      router={this}
      url='/api/summoners/invalid'
      title = 'Invalid Summoners' />;

      React.render(component, element);
    },

    matches: function() {
      var selector = '[data-component=matches]';
      var element = document.querySelector(selector);
      var component = <MatchesComponent url='/api/matches' />;

      React.render(component, element);
    }

  });

  var router = new Router();
  router.history.start({ pushState: true });

  setTimeout(function() {
    $('.alert-box').fadeOut(function() {
      this.remove();
    });
  }, 5000);

  io.on('notifications', function(response) {
    notifier.notify('message', response.message);
  });

  io.on('jobs', function(response) {
    notifier.emit('jobs', response);
  });

  $(window).on('beforeunload', function(){
    io.close();
  });

})();

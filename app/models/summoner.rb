class Summoner
  include Virtus.value_object

  attribute :id, String
  attribute :riot_id, String
  attribute :name, String
  attribute :nick, String
  attribute :team, Team
  attribute :status, Fixnum
  attribute :school, School

  def valid?
    status == 1
  end

  def split_name
    @tokens ||= name.split(' ')
  end

  def first_name
    split_name.first
  end

  def last_name
    split_name.last if split_name.size > 1
  end

  def self.verified
    result = Store.db.table(:summoners)
      .order_by(:name)
      .filter({ status: 1 })
      .run.to_a

    result.map { |s| Summoner.new(s) }
  end

  def self.invalid
    result = Store.db.table(:summoners)
      .order_by(:name)
      .filter({ status: 0 })
      .run.to_a

    result.map { |s| Summoner.new(s) }
  end

end

class Champion
  include Virtus.value_object

  attribute :id, Fixnum
  attribute :key, String
  attribute :name, String
  attribute :title, String

  def image
    "#{name}.png"
  end
end

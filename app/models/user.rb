class User
  include Virtus.model
  include BCrypt

  attribute :id, String
  attribute :email, String
  attribute :password, String

  def name
    email.split('@').first
  end

  def self.authenticate(email, password)
    return nil if email.empty? || password.empty?

    user = Store.db.table(:users).get_all(email, index: :email).run.to_a.first

    if user && Password.new(user['password']) == password
      User.new(id: user['id'], email: email)
    else
      nil
    end
  end

  def self.get(id)
    return User.new(Store.db.table(:users).get(id).run)
  end

  def self.create(email, password)
    is_empty = Store.db.table(:users).get_all(email, index: :email).is_empty.run

    if is_empty
      password = Password.create(password)

      Store.db.table(:users).insert({
        email: email,
        password: password
      }).run
    end
  end
end

class Team
  include Virtus.value_object

  attribute :id, String
  attribute :name, String
  attribute :captain, String
end

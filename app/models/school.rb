class School
  include Virtus.value_object

  attribute :id, String
  attribute :team_id, String
  attribute :name, String
end

class MatchDetail < Riotic::MatchDetail
  include Virtus.model

  attribute :team_purple, String
  attribute :team_blue, String
end

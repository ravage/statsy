class NameCheckWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(payload)
    store = Store.db
    modified = []

    api = Riotic::API.new(
      ENV['RIOT_API_KEY'],
      ENV['RIOT_REGION'],
      Riotic::RedisStore.new
    )

    summoners = payload['summoners']

    summoners = Hash[summoners.map do |s|
      [ s.fetch('nick').downcase.gsub(/\s/, ''), s.fetch('id') ]
    end]

    response = api.summoner.by_name(*summoners.keys)

    response.each do |data|
      next if data.name.nil?

      normalized = data.name.downcase.gsub(/\s/, '')

      next if !summoners.include?(normalized)

      id = summoners.fetch(normalized)

      if summoners.keys.include?(normalized)
        update = data.to_h
        update.delete(:id)
        update[:nick] = update.delete(:name)
        modified << id

        store.table(:summoners).get(id).update(
          update.merge({riot_id: data.id, status: 1 })
        ).run
      else
        store.table(:summoners).get(id).update({ status: 0 }).run
      end

      summoners.delete(normalized)
    end

    message = MultiJson.dump({
      message: "The minions verfified batch
        #{payload['batch']} of #{payload['max']}"
    })

    payload = MultiJson.dump({
      job: 'nameCheck',
      finished: payload['batch'] >= payload['max']
    })

    Notifier.redis.publish('notifications', message)
    Notifier.redis.publish('jobs', payload)
  end
end

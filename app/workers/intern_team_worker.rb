class InternTeamWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    api = Intern::API.new

    api_teams = api.team.teams

    teams = []
    summoners = Set.new

    api_teams.each do |t|
      teams << {
        id: t.id,
        name: t.name,
        captain: t.captain
      }

      summoners << {
        nick: t.captain,
        name: 'N A',
        status: 0,
        team: {
          id:       t.id,
          name:     t.name,
          captain:  t.captain
        }
      }

      t.players.each do |s|
        summoners << {
          nick: s.nick,
          name: s.name,
          status: 0,
          team: {
            id:       t.id,
            name:     t.name,
            captain:  t.captain
          }
        }
      end
    end

    Store.logger = logger

    store = Store.db

    store.table(:teams).insert(teams).run

    db_summoners = store.table(:summoners)[:nick].run.to_a.map do |nick|
      Utils.normalize(nick)
    end

    summoners.reject! do |s|
      db_summoners.include?(Utils.normalize(s.fetch(:nick)))
    end

    store.table(:summoners).insert(summoners.to_a).run

    message = MultiJson.dump({
      message: 'The minions finished importing Teams and Summonners'
    })

    payload = MultiJson.dump({
      job: 'intern',
      finished: true
    })

    Notifier.redis.publish('jobs', payload);
    Notifier.redis.publish('notifications', message)
  end
end

class IdCheckWorker
  include Sidekiq::Worker

  def perform(payload)
    store = Store.db

    api = Riotic::API.new(
      ENV['RIOT_API_KEY'],
      ENV['RIOT_REGION'],
      Riotic::RedisStore.new
    )

    summoners = payload['summoners']

    ids = Hash[summoners.map do |s|
      [ s.fetch('id'), s.fetch('riot_id') ]
    end]

    response = api.summoner.by_id(*ids.values)

    response.each do |summoner|
      store.table(:summoners).get_all(summoner.id, index: :riot_id).update do |row|
        store.branch(
          row[:revision_date].ne(summoner.revision_date),
          {
            nick: summoner.name,
            profile_icon_id: summoner.profile_icon_id,
            revision_date: summoner.revision_date,
            summoner_level: summoner.summoner_level
          },
          {}
        )
      end.run
    end

    message = MultiJson.dump({
      message: "The minions refreshed batch
        #{payload['batch']} of #{payload['max']}"
    })

    payload = MultiJson.dump({
      job: 'idCheck',
      finished: payload['batch'] >= payload['max']
    })

    Notifier.redis.publish('notifications', message)
    Notifier.redis.publish('jobs', payload)
  end
end

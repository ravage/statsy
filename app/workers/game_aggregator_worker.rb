class GameAggregatorWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(payload)
    Store.logger = logger
    store = Store.db

    info = {
      game: payload['match_id'],
      summoner: payload['summoner']['riot_id']
    }

    api = Riotic::API.new(
      ENV['RIOT_API_KEY'],
      ENV['RIOT_REGION'],
      Riotic::RedisStore.new
    )

    begin
      response = api.game.recent(payload['summoner']['riot_id'])
    rescue StandardError => e
      store.table(:exceptions).insert(
        {
          payload: payload,
          exception: e.message,
          source: self.class.name,
          method: __method__,
          created_at: Time.now.utc.to_i
        }
      ).run

      store.table(:matches).get(payload['id']).replace do |m|
        m.without(:status)
      end.run

      logger.info("Exception: #{e} - Info: #{info}")

      return
    end

    game = response.games.select do |g|
      g.game_id == payload['match_id']
    end.first

    if game.nil?
      logger.info("Game not found for #{info}")
      return
    end

    store.table(:games).get(game.game_id).replace do |g|
      store.branch(
        g.eq(nil),
        insert(payload, game),
        store.branch(
          g[:players].contains { |p| p[:summoner_id].eq(payload['summoner']['riot_id']) },
          {},
          g.merge({ players: g[:players].append(player(payload, game)) })
        )
      )
    end.run

    logger.info("Persisting #{info}")
  end

  def insert(payload, game)
    {
      id: game.game_id,
      invalid: game.invalid,
      game_mode: game.game_mode,
      game_type: game.game_type,
      sub_type: game.sub_type,
      map_id: game.map_id,
      create_date: game.create_date,
      duration: payload['duration'],
      players: [player(payload, game)],
    }
  end

  def update(payload, game)
      { players: row[:players].append(player(payload, game)) }
  end

  def player(payload, game)
    {
      summoner_id: payload['summoner']['riot_id'],
      champion_id: game.champion_id,
      riot_team_id: game.team_id,
      internal_team_id: payload['summoner']['team']['id'],
      spell1: game.spell1,
      stats: {
        spell2: game.spell2,
        level: game.level,
        ip_earned: game.ip_earned,
      }.merge(game.stats)
    }
  end

end

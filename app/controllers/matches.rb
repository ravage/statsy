Statsy::App.controllers :matches do
  get :index do
    render 'matches/index'
  end

  before do
    login
  end
end

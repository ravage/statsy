require 'active_support/time'

Statsy::App.controllers :summoners do

  get :verified do
    @summoners = Summoner.verified

    render 'summoners/verified'
  end

  get :invalid do
    @summoners = Summoner.invalid

    render 'summoners/invalid'
  end

  before do
    login
  end
end
